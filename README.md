# SVR-Python

- Recommended video: https://www.youtube.com/watch?v=Y6RRHw9uN9o


Support vector regression (SVR) is a fast and accurate way of interpolating data sets. It is useful when you have an expensive function you want to approximate over a known domain. It learns quickly and is systematically improvable. Variants of SVR are used throughout science including Krigging and Gaussian processes (GP). Support vector regression is a generalization of the support vector machine to the regression problem.
In a classification problem, the vectors are used to define a hyperplane that seperates the two different classes in your solution. [1]

[1] http://mcminis1.github.io/blog/2014/05/10/intuition-for-SVR/
